/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.inject.Inject;


/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */
@FXMLController("/fxml/Delete.fxml")
public class DeleteController {

    @Inject
    private Container container;
    @FXML
    @ActionTrigger("finish")
    private Button yes;
    @FXML
    @ActionTrigger("no")
    private Button no;

    @ActionMethod("change")
    public void change() {
        PatientBookingDao.removePatientbooking(container.getPatientbooking());

        container.setPatientbooking(null);
    }

    @ActionMethod("homepageji")
    public void homepageji() {
        container.setPatientbooking(null);
    }
}
