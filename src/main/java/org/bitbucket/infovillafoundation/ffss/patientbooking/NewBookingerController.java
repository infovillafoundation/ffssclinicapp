/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.util.Callback;
import jfxtras.scene.control.CalendarPicker;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.*;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */
@FXMLController("/fxml/NewBookinger.fxml")
public class NewBookingerController {

    @FXML
    private TextField name;
    @FXML
    private TextField age;
    @FXML
    private TextField sex;
    @FXML
    private TextField farname;
    @FXML

    private TextField nirc;
    @FXML
    private TextField address;
    @FXML
    private DatePicker lbd;
    @FXML
    private Label lbdlabel;
    @FXML
    @ActionTrigger("go")
    private Button done;
    @FXML
    @ActionTrigger("backtable")
    private Button back;
    @FXML
    private ToggleGroup ji;
    @FXML
    private RadioButton male;
    @FXML
    private RadioButton female;
    @FXML
    private Label or;
    @FXML
    @ActionTrigger("allset")
    private Button ok;
    @FXML
    @ActionTrigger("nothing")
    private Button clear;
    @FXML
    @ActionTrigger("to")
    private Button save;
    @FXML
    private Text time;
    @FXML
    @ActionTrigger("createappointment")
    private Button createappointment;

    @FXML
    private CalendarPicker calendar;
    @FXML
    private Text selectdate;
    @FXML
    private Text nexttime;

    private Date appointmentDate;
    private Patientbooking requiredPatient;

    @FXML
    private ComboBox<Doctor> selectdoctor;

    @ActionMethod("allset")
    public void allset() {
        ji.selectToggle(female);
        String patientNirc = nirc.getText();
        List<Patientbooking> patientsWIthNirc = PatientBookingDao.getPatientbookingByNirc(patientNirc);
        requiredPatient = patientsWIthNirc.get(0);
        name.setText(requiredPatient.getName());
        age.setText(requiredPatient.getAge() + "");

        calendar.setVisible(true);

        final List<Calendar> observableList = calendar.highlightedCalendars();

        List<Appointment> appointmentsForRequiredPatient = AppointmentDao.getAllAppointmentLastWeek(requiredPatient);
        List<Appointment> appointmentsForRequiredPatient1 = AppointmentDao.getAllAppointmentNextWeek(requiredPatient);

        for (Appointment appointment : appointmentsForRequiredPatient) {
            Calendar appointmentCalendar = Calendar.getInstance();
            appointmentCalendar.setTime(appointment.getAppointmentDate());
            observableList.add(appointmentCalendar);
        }

        time.setText(appointmentsForRequiredPatient.size() + " appointments last week");

        for (Appointment appointment : appointmentsForRequiredPatient1) {
            Calendar appointmentCalendar = Calendar.getInstance();
            appointmentCalendar.setTime(appointment.getAppointmentDate());
            observableList.add(appointmentCalendar);
        }

        nexttime.setText(appointmentsForRequiredPatient1.size() + " appointments next week");
        calendar.calendarProperty().addListener(new ChangeListener<Calendar>() {
            @Override
            public void changed(ObservableValue<? extends Calendar> observable, Calendar oldValue, Calendar newValue) {
                if (newValue != null) {
                    appointmentDate = newValue.getTime();
                    selectdate.setVisible(true);
                    selectdate.setText(new SimpleDateFormat("dd/MM/yyyy").format(newValue.getTime()));
                }

                createappointment.setVisible(true);
            }
        });

        farname.setText(requiredPatient.getFartherName());
        address.setText(requiredPatient.getAddress());
        lbd.setVisible(false);
        lbdlabel.setVisible(false);
        ok.setVisible(false);
        or.setVisible(false);

        done.setVisible(false);
        save.setVisible(true);
        save.toFront();


        if (requiredPatient.getSex().equals("Male")) {
            ji.getToggles().get(1).setSelected(false);
            ji.getToggles().get(0).setSelected(true);
        }else {
            ji.getToggles().get(0).setSelected(false);
            ji.getToggles().get(1).setSelected(true);
        }
    }

    @ActionMethod("anothersave")
    public void anothersave() {
        Appointment appointment = new Appointment();
        appointment.setAttendence("app");

        appointment.setAppointmentDate(appointmentDate);
        appointment.setDoctor(selectdoctor.getValue());
        List<Appointment> appointments = requiredPatient.getAppointments();
        if (appointments == null) {
            appointments = new ArrayList<Appointment>();
            requiredPatient.setAppointments(appointments);
        }

        appointments.add(appointment);
        //appointment.setPatient(patientbooking);

        AppointmentDao.persistAppointment(requiredPatient, appointment);
        //System.out.println("appointment id: " + appointment.getId());
    }


    @ActionMethod("nothing")
    public void nothing() {
        name.setText("");
        nirc.setText("");
        age.setText("");
        ji.selectToggle(male);
        farname.setText("");
        address.setText("");
    }

    @ActionMethod("save")
    public void save() {
        String patientNirc = nirc.getText();
        List<Patientbooking> patientsWIthNirc = PatientBookingDao.getPatientbookingByNirc(patientNirc);
        Patientbooking requiredPatient = patientsWIthNirc.get(0);
        requiredPatient.setName(name.getText());
        requiredPatient.setAge(Integer.parseInt(age.getText()));
        if (ji.getSelectedToggle() == male) {
            System.out.println("male");
            requiredPatient.setSex("Male");
        } else {
            requiredPatient.setSex("Female");
        }
        requiredPatient.setFartherName(farname.getText());
        requiredPatient.setNirc(nirc.getText());
        requiredPatient.setAddress(address.getText());

        PatientBookingDao.mergePatientbooking(requiredPatient);


    }

    @ActionMethod("set")
    public void set() {

        Patientbooking patientbooking = new Patientbooking();
        patientbooking.setName(name.getText());
        patientbooking.setAge(Integer.parseInt(age.getText()));
        if (ji.getSelectedToggle() == male) {
            patientbooking.setSex("Male");
        } else {
            patientbooking.setSex("Female");
        }



        patientbooking.setFartherName(farname.getText());
        patientbooking.setNirc(nirc.getText());
        patientbooking.setAddress(address.getText());

        LocalDate localDate = lbd.getValue();
        if (localDate != null) {
            Appointment appointment = new Appointment();
            appointment.setAttendence("app");
            Date utilDate = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            appointment.setAppointmentDate(utilDate);
            appointment.setDoctor(selectdoctor.getValue());
            List<Appointment> appointments = patientbooking.getAppointments();
            if (appointments == null) {
                appointments = new ArrayList<Appointment>();
                patientbooking.setAppointments(appointments);
            }
            appointments.add(appointment);
            //appointment.setPatient(patientbooking);

            AppointmentDao.persistAppointment(patientbooking, appointment);
            //System.out.println("appointment id: " + appointment.getId());
        } else {
            AppointmentDao.persistAppointment(patientbooking);
        }

    }

    @PostConstruct
    public void init() {

        calendar.setVisible(false);
        selectdate.setVisible(false);
        ji.selectToggle(male);
        ok.setVisible(false);
        createappointment.setVisible(false);
        nirc.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (nirc.getText().length() >= 14) {
                    List<Patientbooking> patientbookings = PatientBookingDao.getPatientbookingByNirc(nirc.getText());

                    if (patientbookings == null || patientbookings.size() == 0) {
                        or.setText("");
                        ok.setVisible(false);
                        //or.setText("OK");
                    } else {
                        /*Patientbooking patientnbooking = patientbookings.get(0);
                         List<Appointment> appointments = patientnbooking.getAppointments();
                         System.out.println("appointments: " + appointments);
                         Appointment latestAppointment = appointments.get(0);

                         for (int i = 0; i < appointments.size(); i++) {
                         if (latestAppointment.getAppointmentDate().getTime() < appointments.get(i).getAppointmentDate().getTime())
                         latestAppointment = appointments.get(i);
                         }

                         Date today = new Date();
                         long todayMs = today.getTime();

                         long latestAppointmentDateMs = latestAppointment.getAppointmentDate().getTime();

                         long sixDaysMs = 1000 * 3600 * 24 * 6;

                         if (latestAppointmentDateMs + sixDaysMs > todayMs) {
                         or.setText("Less than one week");
                         ok.setVisible(true);
                         } else {
                         or.setText("Existing patient");
                         ok.setVisible(true);
                         }*/
                        or.setText("Patient exists in the database. Load?");
                        ok.setVisible(true);
                    }

                } else {
                    or.setText("");
                    ok.setVisible(false);
                }
            }

        });

        List<Doctor> doctors = DoctorDao.getAllDoctor();
       selectdoctor.getItems().setAll(doctors);
        selectdoctor.setButtonCell(new ListCell<Doctor>() {
            @Override
            protected void updateItem(Doctor item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty)
                    setText(item.getDoctorName());
            }
        });
        selectdoctor.setCellFactory(new Callback<ListView<Doctor>, ListCell<Doctor>>() {
            @Override
            public ListCell<Doctor> call(ListView<Doctor> param) {
                return new ListCell<Doctor>() {
                    @Override
                    protected void updateItem(Doctor item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty)
                        setText(item.getDoctorName());
                    }
                };
            }
        });
    }

}
