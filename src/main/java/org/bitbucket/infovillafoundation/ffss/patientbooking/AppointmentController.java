/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Appointment;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;


/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */

@FXMLController("/fxml/Appointment.fxml")
public class AppointmentController {

    @FXML
    private TableColumn<Appointment, Long> id;
    @FXML
    private TableColumn<Appointment, Date> appointmentdate;
    @FXML
    private TableColumn<Appointment, String> attendence;
    @FXML
    private TableColumn<Appointment, Doctor> doctor;
    @FXML
    private TableView at;
    @FXML
    @ActionTrigger("goandcreate")
    private Button create;
    @FXML
    @ActionTrigger("godelete")
    private Button delete;
    @FXML
    @ActionTrigger("goeditto")
    private Button edit;
    @FXML
    @ActionTrigger("goback")
    private Button back;

    @Inject
    private Container container;

    @PostConstruct
    public void init() {
        id.setCellValueFactory(new PropertyValueFactory<Appointment, Long>("id"));
        attendence.setCellValueFactory(new PropertyValueFactory<Appointment, String>("attendence"));
        appointmentdate.setCellValueFactory(new PropertyValueFactory<Appointment, Date>("appointmentDate"));
        doctor.setCellValueFactory(new PropertyValueFactory<Appointment, Doctor>("doctor"));

        doctor.setCellFactory(new Callback<TableColumn<Appointment, Doctor>, TableCell<Appointment, Doctor>>() {
            @Override
            public TableCell<Appointment, Doctor> call(TableColumn<Appointment, Doctor> param) {
                return new TableCell<Appointment, Doctor>() {
                    @Override
                    protected void updateItem(Doctor item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty && item != null) {
                            setText(item.getDoctorName());
                        }
                    }
                };
            }
        });

        container.setPatientbooking(PatientBookingDao.refreshPatientbooking(container.getPatientbooking()));
        List<Appointment> appointments = container.getPatientbooking().getAppointments();
        at.getItems().setAll(appointments);

        at.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Appointment>() {

            @Override
            public void changed(ObservableValue<? extends Appointment> observable, Appointment oldValue, Appointment newValue) {
                container.setAppointment(newValue);
                System.out.println("LBD  :" + newValue.getAppointmentDate());
            }

        });

    }
}
