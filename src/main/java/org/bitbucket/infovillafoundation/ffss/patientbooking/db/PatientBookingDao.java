package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author TOSHIBA
 */
public class PatientBookingDao extends Dao {

    public static void persistPatientBooking(Patientbooking patientbooking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(patientbooking);
        em.getTransaction().commit();
        em.close();
    }


    public static List<Patientbooking> getAllPatientbooking() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Patientbooking> PatientbookingQuery = em.createNamedQuery("Patientbooking.findAll", Patientbooking.class);
        List<Patientbooking> Patientbooking = PatientbookingQuery.getResultList();
        em.close();
        return Patientbooking;
    }
    public static List<Patientbooking> getAllPatientbookings(Doctor doctor,Date appointmentDate) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Patientbooking> patientbookingQuery = em.createNamedQuery("Patientbooking.findByDoctorAndAppointmentDate", Patientbooking.class);
        patientbookingQuery.setParameter("doctor" , doctor);
        patientbookingQuery.setParameter("appointmentDate" ,appointmentDate);
        List<Patientbooking> patientbookings = patientbookingQuery.getResultList();
        em.close();
        return patientbookings;
    }

    public static List<Patientbooking> getPatientbookingByNirc(String nirc) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Patientbooking> PatientbookingQuery = em.createNamedQuery("Patientbooking.findByNircLike", Patientbooking.class);
        PatientbookingQuery.setParameter("nirc", "%" + nirc + "%");
        List<Patientbooking> Patientbookings = PatientbookingQuery.getResultList();
        em.close();
        return Patientbookings;
    }

    public static void removePatientbooking(Patientbooking patientbooking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Patientbooking patientbooking1ToDelete = em.find(Patientbooking.class, patientbooking.getId());
        em.remove(patientbooking1ToDelete);
        em.getTransaction().commit();
        em.close();
    }

    public static Patientbooking refreshPatientbooking(Patientbooking patientbooking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Patientbooking patientbooking1ToRefresh = em.find(Patientbooking.class, patientbooking.getId());
        em.refresh(patientbooking1ToRefresh);
        em.getTransaction().commit();
        em.close();
        return patientbooking1ToRefresh;
    }


    public static void mergePatientbooking(Patientbooking patientbooking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Patientbooking patientbooking1ToMerge = em.find(Patientbooking.class, patientbooking.getId());
        patientbooking1ToMerge.setId(patientbooking.getId());
        patientbooking1ToMerge.setName(patientbooking.getName());
        patientbooking1ToMerge.setAge(patientbooking.getAge());
        patientbooking1ToMerge.setFartherName(patientbooking.getFartherName());
        patientbooking1ToMerge.setNirc(patientbooking.getNirc());
        patientbooking1ToMerge.setSex(patientbooking.getSex());
        patientbooking1ToMerge.setAddress(patientbooking.getAddress());
        em.merge(patientbooking1ToMerge);
        em.getTransaction().commit();
        em.close();
    }
}
