package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.DoctorDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import java.awt.*;


/**
 * Created by TOSHIBA on 12/21/2014.
 */
@FXMLController("/fxml/NewDoctor.fxml")
public class NewDoctorController {

    @FXML
    private TextField id;
    @FXML
    private TextField doctorname;
    @FXML
    private TextField type;
    @FXML
    private Button save;

    @FXML
    @ActionTrigger("backtodoctor")
    private Button finish;

    @ActionMethod("finish")
    public void finish() {
        Doctor doctor = new Doctor();

        doctor.setDoctorName(doctorname.getText());
        doctor.setType(type.getText());

        DoctorDao.persistDoctor(doctor);
    }
    @PostConstruct
    public void init() {
        save.setVisible(false);
    }
}
