/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.List;

/**
 * @author TOSHIBA
 */
public class AppointmentDao extends Dao {
    public static void persistAppointment(Patientbooking patientbooking, Appointment appointment) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(appointment);
        em.merge(patientbooking);
        em.getTransaction().commit();
        em.refresh(appointment);
        em.close();
    }

    public static void persistAppointment(Patientbooking patientbooking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(patientbooking);
        em.getTransaction().commit();
        em.close();
    }

    public static List<Appointment> getAllAppointment() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Appointment> AppointmentQuery = em.createNamedQuery("Appointment.findAll", Appointment.class);
        List<Appointment> appointments = AppointmentQuery.getResultList();
        em.close();
        return appointments;
    }

    public static List<Appointment> getAllAppointmentLastWeek(Patientbooking patient) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Appointment> appointmentQuery = em.createNamedQuery("Appointment.findByAppointmentDateBetween", Appointment.class);
        Calendar sevenDaysBefore = Calendar.getInstance();
        sevenDaysBefore.add(Calendar.DATE, -7);
        Calendar today = Calendar.getInstance();
        appointmentQuery.setParameter("date1", sevenDaysBefore.getTime());
        appointmentQuery.setParameter("date2", today.getTime());
        appointmentQuery.setParameter("patient", patient);
        List<Appointment> appointments = appointmentQuery.getResultList();
        em.close();
        return appointments;
    }
    public static List<Appointment> getAllAppointmentNextWeek(Patientbooking patient) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Appointment> appointmentQuery = em.createNamedQuery("Appointment.findByAppointmentDateBetween", Appointment.class);
        Calendar sevenDaysAfter = Calendar.getInstance();
        sevenDaysAfter.add(Calendar.DATE, 7);
        Calendar today1 = Calendar.getInstance();
        appointmentQuery.setParameter("date1", today1.getTime());
        appointmentQuery.setParameter("date2", sevenDaysAfter.getTime());
        appointmentQuery.setParameter("patient", patient);
        List<Appointment> appointments = appointmentQuery.getResultList();
        em.close();
        return appointments;
    }

    public static void removeAppointment(Patientbooking patientbooking, Appointment appointment) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Patientbooking parentPatientbooking = em.find(Patientbooking.class, patientbooking.getId());
        Appointment appointmentToDelete = em.find(Appointment.class, appointment.getId());
        parentPatientbooking.getAppointments().remove(appointment);
        em.remove(appointmentToDelete);
        em.merge(parentPatientbooking);
        em.getTransaction().commit();
        em.close();
    }

}
