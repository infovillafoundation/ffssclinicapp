package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.DoctorDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Patientbooking;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;


/**
 * Created by TOSHIBA on 12/21/2014.
 */
@FXMLController("/fxml/doctor.fxml")
public class DoctorController {
    ;
    @FXML
    private TableView DT;
    @FXML
    private TableColumn<Doctor ,Long> id;
    @FXML
    private TableColumn <Doctor ,String> doctorname;
    @FXML
    private TableColumn <Doctor ,String> type;
    @FXML
    @ActionTrigger("tonewdoctor")
    private Button newdoctor;
    @FXML
    @ActionTrigger("deletepage")
    private Button delete;
    @FXML
    @ActionTrigger("editdoctor")
    private Button edit;
    @FXML
    @ActionTrigger("backfromdoctor")
    private Button back;
    @FXML
    private DatePicker searchdoctor;
    @FXML
    @ActionTrigger("seepatientlist")
    private Button seepatient;

    @Inject
    private Container container;

    private Doctor doctor;

    @PostConstruct
    public void init() {
        id.setCellValueFactory(new PropertyValueFactory<Doctor, Long>("id"));
        doctorname.setCellValueFactory(new PropertyValueFactory<Doctor, String>("doctorName"));
        type.setCellValueFactory(new PropertyValueFactory<Doctor, String>("type"));

        List<Doctor> doctors = DoctorDao.getAllDoctor();
        DT.getItems().setAll(doctors);

        DT.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Doctor>() {


            @Override
            public void changed(ObservableValue<? extends Doctor> observable, Doctor oldValue, Doctor newValue) {
                doctor = newValue;
                container.setDoctor(newValue);
                System.out.println("doctor: " + newValue.getDoctorName());
            }
        });
    }
    @ActionMethod("openpatientlist")
    public void openpatientlist() {

        LocalDate localDate = searchdoctor.getValue();
        Date utilDate = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        container.setAppointmentDate(utilDate);
        List<Patientbooking> patientBookings = PatientBookingDao.getAllPatientbookings(doctor,utilDate);
        container.setPatientList(patientBookings);
    }
}
