/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.application.Application;
import javafx.stage.Stage;
import org.datafx.controller.flow.Flow;
import org.datafx.controller.flow.action.FlowActionChain;
import org.datafx.controller.flow.action.FlowLink;
import org.datafx.controller.flow.action.FlowMethodAction;

/**
 * @author TOSHIBA
 */
public class PatientBooking extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        new Flow(PatientBookingTableController.class)
                .withAction(PatientBookingTableController.class, "bookinger", new FlowLink(NewBookingerController.class))
                .withAction(NewBookingerController.class, "go", new FlowActionChain(new FlowMethodAction("set"), new FlowLink(PatientBookingTableController.class)))
                .withAction(NewBookingerController.class, "to", new FlowActionChain(new FlowMethodAction("save"), new FlowLink(PatientBookingTableController.class)))
                .withAction(PatientBookingTableController.class, "look", new FlowActionChain(new FlowMethodAction("detail"), new FlowLink(DetailController.class)))
                .withAction(DetailController.class, "back", new FlowActionChain(new FlowMethodAction("toNoMethod"), new FlowLink(PatientBookingTableController.class)))
                .withAction(PatientBookingTableController.class, "delete", new FlowActionChain(new FlowMethodAction("detail"), new FlowLink(DeleteController.class)))
                .withAction(DeleteController.class, "finish", new FlowActionChain(new FlowMethodAction("change"), new FlowLink(PatientBookingTableController.class)))
                .withAction(DeleteController.class, "no", new FlowActionChain(new FlowMethodAction("homepageji"), new FlowLink(PatientBookingTableController.class)))
                .withAction(PatientBookingTableController.class, "edit", new FlowActionChain(new FlowMethodAction("detail"), new FlowLink(EditController.class)))
                .withAction(NewBookingerController.class, "backtable", new FlowLink(PatientBookingTableController.class))
                .withAction(EditController.class, "backtableby", new FlowLink(PatientBookingTableController.class))
                .withAction(EditController.class, "backtablebyedit", new FlowActionChain(new FlowMethodAction("zone"), new FlowLink(PatientBookingTableController.class)))
                .withAction(DetailController.class, "goappointment", new FlowLink(AppointmentController.class))
                .withAction(AppointmentController.class, "goandcreate", new FlowLink(CreateAppointmentController.class))
                .withAction(CreateAppointmentController.class, "ok", new FlowActionChain(new FlowMethodAction("affect"), new FlowLink(AppointmentController.class)))
                .withAction(AppointmentController.class, "godelete", new FlowLink(AppointmentdeleteController.class))
                .withAction(AppointmentController.class, "goeditto", new FlowLink(AppointmenteditController.class))
                .withAction(AppointmentController.class, "goback", new FlowLink(PatientBookingTableController.class))
                .withAction(CreateAppointmentController.class, "toback", new FlowLink(AppointmentController.class))
                .withAction(PatientBookingTableController.class, "todoctor", new FlowLink(DoctorController.class))
                .withAction(DoctorController.class, "tonewdoctor", new FlowLink(NewDoctorController.class))
                .withAction(DoctorController.class, "deletepage", new FlowLink(DeleteDoctorController.class))
                .withAction(DoctorController.class, "editdoctor", new FlowLink(EditDoctorController.class))
                .withAction(DoctorController.class, "backfromdoctor", new FlowLink(PatientBookingTableController.class))
                .withAction(SeePatientController.class, "backfromseepatient", new FlowLink(DoctorController.class))
                .withAction(DeleteDoctorController.class, "yes",new FlowActionChain(new FlowMethodAction("letsdelete"), new FlowLink(DoctorController.class)))
                .withAction(DoctorController.class, "seepatientlist",new FlowActionChain(new FlowMethodAction("openpatientlist"), new FlowLink(SeePatientController.class)))
                .withAction(EditDoctorController.class, "editting",new FlowActionChain(new FlowMethodAction("saveeditting"), new FlowLink(DoctorController.class)))
                .withAction(DeleteDoctorController.class, "letsnotdelete", new FlowLink(DoctorController.class))
                .withAction(NewDoctorController.class, "backtodoctor", new FlowActionChain(new FlowMethodAction("finish"), new FlowLink(DoctorController.class)))
                .withAction(NewBookingerController.class, "createappointment", new FlowActionChain(new FlowMethodAction("anothersave"), new FlowLink(PatientBookingTableController.class)))
                .withAction(AppointmentdeleteController.class, "yes", new FlowActionChain(new FlowMethodAction("todelete"), new FlowLink(AppointmentController.class)))
                .withAction(AppointmentdeleteController.class, "undo", new FlowActionChain(new FlowMethodAction("dontdo"), new FlowLink(AppointmentController.class)))
                .startInStage(stage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}