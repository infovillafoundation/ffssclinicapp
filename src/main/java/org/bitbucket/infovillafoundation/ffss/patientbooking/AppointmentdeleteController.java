/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.AppointmentDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.inject.Inject;


@FXMLController("/fxml/Appointmentdelete.fxml")
public class AppointmentdeleteController {
    @Inject
    private Container container;
    @FXML
    @ActionTrigger("yes")
    private Button yes;
    @FXML
    @ActionTrigger("undo")
    private Button no;

    @ActionMethod("todelete")
    public void todelete() {

        AppointmentDao.removeAppointment(container.getPatientbooking(), container.getAppointment());

        container.setAppointment(null);
    }

    @ActionMethod("dontdo")
    public void dontdo() {
        container.setAppointment(null);
    }
}
