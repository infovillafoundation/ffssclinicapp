package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

/**
 * Created by TOSHIBA on 12/21/2014.
 */

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "doctors" )
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Doctor.findAll", query="SELECT d FROM Doctor d "),
        @NamedQuery(name = "Doctor.findById", query = "SELECT d FROM Doctor d WHERE d.id = :id"),
        @NamedQuery(name = "Doctor.findByDoctor_name", query = "SELECT d FROM Doctor d WHERE d.id = :doctorName"),
        @NamedQuery(name = "Doctor.findByType", query = "SELECT d FROM Doctor d WHERE d.id = :type")})

public class Doctor implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "Doctor_name")
    private String doctorName;
    @Basic(optional = false)
    @Column(name = "Type")
    private String type;


    public Doctor() {
    }

    public Doctor(Long id) {
        this.id = id;
    }

    public Doctor (Long id,String doctorName, String type){
        this.id = id;
        this.doctorName = doctorName;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}


