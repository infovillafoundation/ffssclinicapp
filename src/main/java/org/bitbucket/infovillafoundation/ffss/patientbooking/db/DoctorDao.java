package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by TOSHIBA on 12/21/2014.
 */
public class DoctorDao extends Dao {
    public static void persistDoctor(Doctor doctor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(doctor);
        em.getTransaction().commit();
        em.close();
    }

    public static List<Doctor> getAllDoctor() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Doctor> DoctorQuery = em.createNamedQuery("Doctor.findAll", Doctor.class);
        List<Doctor> Doctor = DoctorQuery.getResultList();
        em.close();
        return Doctor;


}
    public static void removeDoctor(Doctor doctor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Doctor doctorToDelete1 = em.find(Doctor.class, doctor.getId());
        em.remove(doctorToDelete1);
        em.getTransaction().commit();
        em.close();
    }

    public static void mergeDoctor(Doctor doctor) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Doctor doctorToMerge = em.find(Doctor.class, doctor.getId());
        doctorToMerge.setId(doctor.getId());
        doctorToMerge.setDoctorName(doctor.getDoctorName());
        doctorToMerge.setType(doctor.getType());
        em.merge(doctorToMerge);
        em.getTransaction().commit();
        em.close();
    }

        }
