/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * @author TOSHIBA
 */
@Entity
@Table(name = "appointments")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Appointment.findAll", query = "SELECT a FROM Appointment a"),
        @NamedQuery(name = "Appointment.findById", query = "SELECT a FROM Appointment a WHERE a.id = :id"),
        @NamedQuery(name = "Appointment.findByAppointmentDate", query = "SELECT a FROM Appointment a WHERE a.appointmentDate = :appointmentDate"),
        @NamedQuery(name = "Appointment.findByAppointmentDateBetween", query = "SELECT a FROM Appointment a WHERE a.appointmentDate BETWEEN :date1 AND :date2 AND a.patient = :patient"),
        @NamedQuery(name = "Appointment.findByAttendence", query = "SELECT a FROM Appointment a WHERE a.attendence = :attendence")})
        @NamedQuery(name = "Appointment.findByDoctorAndAppointmentDate",query = "SELECT a FROM Appointment a WHERE a.doctor =:doctor AND a.appointmentDate=:appointmentDate")

public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "appointment_date")
    @Temporal(TemporalType.DATE)
    private Date appointmentDate;

    @Basic(optional = false)
    @Column(name = "attendence")
    private String attendence;

    @OneToOne
    @JoinColumn(name = "doctor_id")
        private Doctor doctor;

    @ManyToOne
    @JoinTable
            (
                    name = "patientbooking_appointments",
                    joinColumns = {@JoinColumn(name = "appointments_id", referencedColumnName = "id" ,insertable = false, updatable = false)},
                    inverseJoinColumns = {@JoinColumn(name = "patientbooking_id", referencedColumnName = "id", insertable = false, updatable = false)}
            )
    private Patientbooking patient;

    public Appointment() {
    }

    public Appointment(Long id) {
        this.id = id;
    }

    public Appointment(Long id, Date appointmentDate, String attendence, long patientId) {
        this.id = id;
        this.appointmentDate = appointmentDate;
        this.attendence = attendence;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAttendence() {
        return attendence;
    }

    public void setAttendence(String attendence) {
        this.attendence = attendence;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointment)) {
            return false;
        }
        Appointment other = (Appointment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "patientbookingdb.Appointments[ id=" + id + " ]";
    }

}
