package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Appointment;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.AppointmentDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.DoctorDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@FXMLController("/fxml/CreateAppointment.fxml")
public class CreateAppointmentController {

    @Inject
    private Container container;


    @FXML
    private DatePicker date;

    @FXML
    @ActionTrigger("ok")
    private Button ok;
    @FXML
    @ActionTrigger("toback")
    private Button back;
    @FXML
    private ComboBox<Doctor> selectdoctor;

    @ActionMethod("affect")
    public void affect() {
        Appointment appointment = new Appointment();

        LocalDate localDate = date.getValue();
        Date utilDate = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        appointment.setAppointmentDate(utilDate);
        appointment.setAttendence("app");
        appointment.setDoctor(selectdoctor.getValue());
        container.getPatientbooking().getAppointments().add(appointment);

        AppointmentDao.persistAppointment(container.getPatientbooking(), appointment);


    }

    @PostConstruct
    public void init() {
        List<Doctor> doctors = DoctorDao.getAllDoctor();
        selectdoctor.getItems().setAll(doctors);
        selectdoctor.setButtonCell(new ListCell<Doctor>() {
            @Override
            protected void updateItem(Doctor item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty)
                    setText(item.getDoctorName());
            }
        });
        selectdoctor.setCellFactory(new Callback<ListView<Doctor>, ListCell<Doctor>>() {
            @Override
            public ListCell<Doctor> call(ListView<Doctor> param) {
                return new ListCell<Doctor>() {
                    @Override
                    protected void updateItem(Doctor item, boolean empty) {
                        super.updateItem(item, empty);
                        if (!empty)
                            setText(item.getDoctorName());
                    }
                };
            }
        });
    }
}
