package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.DoctorDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


/**
 * Created by TOSHIBA on 12/21/2014.
 */
@FXMLController("/fxml/NewDoctor.fxml")
public class EditDoctorController {

    @FXML
    private TextField doctorname;
    @FXML
    private TextField type;
    @FXML
    @ActionTrigger("editting")
    private Button save;
    @FXML
    private Button finish;
    @Inject
    private Container container;

    @PostConstruct
    public void init() {
        doctorname.setText(container.getDoctor().getDoctorName());
        type.setText(container.getDoctor().getType());

        finish.setVisible(false);
        save.setVisible(true);
    }
    @ActionMethod("saveeditting")
    public void saveeditting() {
        container.getDoctor().setDoctorName(doctorname.getText());
        container.getDoctor().setType(type.getText());

        DoctorDao.mergeDoctor(container.getDoctor());

        container.setDoctor(null);
    }
}
