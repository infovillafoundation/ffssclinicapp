package org.bitbucket.infovillafoundation.ffss.patientbooking.db;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * @author TOSHIBA
 */
@Entity
@Table(name = "patientbooking")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Patientbooking.findAll", query = "SELECT p FROM Patientbooking p"),
        @NamedQuery(name = "Patientbooking.findById", query = "SELECT p FROM Patientbooking p WHERE p.id = :id"),
        @NamedQuery(name = "Patientbooking.findByName", query = "SELECT p FROM Patientbooking p WHERE p.name = :name"),
        @NamedQuery(name = "Patientbooking.findByAge", query = "SELECT p FROM Patientbooking p WHERE p.age = :age"),
        @NamedQuery(name = "Patientbooking.findBySex", query = "SELECT p FROM Patientbooking p WHERE p.sex = :sex"),
        @NamedQuery(name = "Patientbooking.findByFartherName", query = "SELECT p FROM Patientbooking p WHERE p.fartherName = :fartherName"),
        @NamedQuery(name = "Patientbooking.findByNirc", query = "SELECT p FROM Patientbooking p WHERE p.nirc = :nirc"),
        @NamedQuery(name = "Patientbooking.findByAddress", query = "SELECT p FROM Patientbooking p WHERE p.address = :address"),
        @NamedQuery(name = "Patientbooking.findByNircLike", query = "SELECT p FROM Patientbooking p WHERE p.nirc Like :nirc")})
        @NamedQuery(name = "Patientbooking.findByDoctorAndAppointmentDate",query = "SELECT p FROM Patientbooking p JOIN p.appointments a WHERE a.doctor=:doctor AND a.appointmentDate = :appointmentDate")


public class Patientbooking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "Name")
    private String name;
    @Basic(optional = false)
    @Column(name = "Age")
    private long age;
    @Basic(optional = false)
    @Column(name = "Sex")
    private String sex;
    @Basic(optional = false)
    @Column(name = "Farther_Name")
    private String fartherName;
    @Basic(optional = false)
    @Column(name = "Nirc")
    private String nirc;
    @Basic(optional = false)
    @Column(name = "Address")
    private String address;
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
    @JoinTable
            (
                    name = "patientbooking_appointments",
                    joinColumns = {@JoinColumn(name = "patientbooking_id", referencedColumnName = "id")},
                    inverseJoinColumns = {@JoinColumn(name = "appointments_id", referencedColumnName = "id", unique = true)}
            )

                    private List<Appointment> appointments;

    public Patientbooking() {
    }

    public Patientbooking(Long id) {
        this.id = id;
    }

    public Patientbooking(Long id, String name, long age, String sex, String fartherName, String nirc, String address, String lbd) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.fartherName = fartherName;
        this.nirc = nirc;
        this.address = address;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getFartherName() {
        return fartherName;
    }

    public void setFartherName(String fartherName) {
        this.fartherName = fartherName;
    }

    public String getNirc() {
        return nirc;
    }

    public void setNirc(String nirc) {
        this.nirc = nirc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patientbooking)) {
            return false;
        }
        Patientbooking other = (Patientbooking) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "patientbookingdb.Patientbooking[ id=" + id + " ]";
    }

}
