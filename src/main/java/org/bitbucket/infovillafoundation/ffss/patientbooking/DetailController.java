/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */
@FXMLController("/fxml/Detail.fxml")
public class DetailController {
    @FXML
    private Text id;
    @FXML
    private Text name;
    @FXML
    private Text age;
    @FXML
    private Text sex;
    @FXML
    private Text farname;
    @FXML
    private Text nirc;
    @FXML
    private Text address;
    @FXML
    @ActionTrigger("back")
    private Button back;
    @FXML
    @ActionTrigger("goappointment")
    private Button ba;

    @Inject
    private Container container;

    @PostConstruct
    public void start() {
        id.setText(container.getPatientbooking().getId().toString());
        name.setText(container.getPatientbooking().getName());
        age.setText(container.getPatientbooking().getAge() + "");
        sex.setText(container.getPatientbooking().getSex());
        farname.setText(container.getPatientbooking().getFartherName());
        nirc.setText(container.getPatientbooking().getNirc());
        address.setText(container.getPatientbooking().getAddress());


    }

    @ActionMethod("toNoMethod")
    public void toNoMethod() {
        container.setPatientbooking(null);
    }


}
