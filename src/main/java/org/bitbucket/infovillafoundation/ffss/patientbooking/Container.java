package org.bitbucket.infovillafoundation.ffss.patientbooking;

import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Appointment;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Patientbooking;
import org.datafx.controller.flow.injection.FlowScoped;

import java.util.Date;
import java.util.List;

@FlowScoped
public class Container {

    private Date appointmentDate;

    public Date getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public List<Patientbooking> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patientbooking> patientList) {
        this.patientList = patientList;
    }

    private List<Patientbooking> patientList;

    private Patientbooking patientbooking;

    public Patientbooking getPatientbooking() {
        return patientbooking;
    }

    public void setPatientbooking(Patientbooking patientbooking) {
        this.patientbooking = patientbooking;
    }

    private Doctor doctor;

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    private Appointment appointment;

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }



}
