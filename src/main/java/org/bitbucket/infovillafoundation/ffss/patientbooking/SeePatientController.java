package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Appointment;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Patientbooking;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by TOSHIBA on 12/22/2014.
 */
@FXMLController("/fxml/Seepatient.fxml")
public class SeePatientController {
    @FXML
    private TableView<Patientbooking> st;
    @FXML
    private TableColumn<Patientbooking,String>patientname;
    @FXML
    private TableColumn<Patientbooking,String>patientnirc;
    @FXML
    @ActionTrigger("backfromseepatient")
    private Button back;
    @Inject
    private Container container;
    @PostConstruct
    public void init() {
        patientname.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("name"));
        patientnirc.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("nirc"));
        System.out.println(st.getItems());
        st.getItems().setAll(container.getPatientList());
    }
}
