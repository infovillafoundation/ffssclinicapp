/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Patientbooking;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;


/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */
@FXMLController("/fxml/PatientBookingTable.fxml")
public class PatientBookingTableController {

    @FXML
    @ActionTrigger("bookinger")
    private Button bookinger;
    @FXML
    private TableColumn<Patientbooking, Long> id;
    @FXML
    private TableColumn<Patientbooking, String> name;
    @FXML
    private TableColumn<Patientbooking, Integer> age;
    @FXML
    private TableColumn<Patientbooking, String> sex;
    @FXML
    private TableColumn<Patientbooking, String> farname;
    @FXML
    private TableColumn<Patientbooking, String> nirc;
    @FXML
    private TableColumn<Patientbooking, String> address;

    @FXML
    private TableView t;
    @FXML
    private TextField searchnirc;
    @FXML
    @ActionTrigger("look")
    private Button detail;
    @FXML
    @ActionTrigger("delete")
    private Button delete;
    @FXML
    @ActionTrigger("edit")
    private Button edit;
    @FXML
    @ActionTrigger("todoctor")
    private Button doctor;

    @Inject
    private Container container;

    @PostConstruct
    public void init() {
        id.setCellValueFactory(new PropertyValueFactory<Patientbooking, Long>("id"));
        name.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("name"));
        age.setCellValueFactory(new PropertyValueFactory<Patientbooking, Integer>("age"));
        sex.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("sex"));
        farname.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("fartherName"));
        nirc.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("nirc"));
        address.setCellValueFactory(new PropertyValueFactory<Patientbooking, String>("address"));


        List<Patientbooking> patientbookings = PatientBookingDao.getAllPatientbooking();
        t.getItems().setAll(patientbookings);

        t.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Patientbooking>() {

            @Override
            public void changed(ObservableValue<? extends Patientbooking> observable, Patientbooking oldValue, Patientbooking newValue) {
                container.setPatientbooking(newValue);
                System.out.println("patient: " + newValue.getName());
            }

        });

        searchnirc.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                List<Patientbooking> patientbookingsList = PatientBookingDao.getPatientbookingByNirc(newValue);
                t.getItems().setAll(patientbookingsList);
            }

        });

        t.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Patientbooking>() {

            @Override
            public void changed(ObservableValue<? extends Patientbooking> observable, Patientbooking oldValue, Patientbooking newValue) {
                container.setPatientbooking(newValue);
                System.out.println("patient: " + newValue.getName());
            }

        });


    }

    @ActionMethod("detail")
    public void detail() {

        if (container.getPatientbooking() == null)
            throw new RuntimeException("cannot pass null to next page");

    }

}
