package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.Doctor;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.DoctorDao;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.inject.Inject;

/**
 * Created by TOSHIBA on 12/21/2014.
 */
@FXMLController("/fxml/Deletedoctor.fxml")
public class DeleteDoctorController {
    @Inject
    private Container container;
    @FXML
    @ActionTrigger("yes")
    private Button yes;
    @FXML
    @ActionTrigger("letsnotdelete")
    private Button no;
    @ActionMethod("letsdelete")
    public void letsdelete() {
        DoctorDao.removeDoctor(container.getDoctor());

        container.setDoctor(null);
    }
}
