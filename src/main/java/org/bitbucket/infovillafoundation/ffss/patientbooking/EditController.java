/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bitbucket.infovillafoundation.ffss.patientbooking;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import jfxtras.scene.control.CalendarPicker;
import org.bitbucket.infovillafoundation.ffss.patientbooking.db.PatientBookingDao;
import org.datafx.controller.FXMLController;
import org.datafx.controller.flow.action.ActionMethod;
import org.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


/**
 * FXML Controller class
 *
 * @author TOSHIBA
 */
@FXMLController("/fxml/NewBookinger.fxml")
public class EditController {

    @FXML
    private TextField id;
    @FXML
    private TextField name;
    @FXML
    private TextField age;
    @FXML
    private TextField farname;
    @FXML
    private TextField nirc;
    @FXML
    private TextField address;
    @FXML
    private RadioButton male;
    @FXML
    private RadioButton female;
    @FXML
    private ToggleGroup ji;
    @FXML
    private DatePicker lbd;
    @FXML
    private Label lbdlabel;
    @FXML
    @ActionTrigger("backtableby")
    private Button back;
    @FXML
    @ActionTrigger("backtablebyedit")
    private Button done;

    @FXML
    private Button ok;
    @FXML
    private Button clear;
    @FXML
    private Button save;
    @FXML
    private Text time;
    @FXML
    private Button createappointment;

    @FXML
    private CalendarPicker calendar;

    @Inject
    private Container container;

    @PostConstruct
    public void init() {

        name.setText(container.getPatientbooking().getName());
        age.setText(container.getPatientbooking().getAge() + "");
        farname.setText(container.getPatientbooking().getFartherName());
        nirc.setText(container.getPatientbooking().getNirc().toString());
        address.setText(container.getPatientbooking().getAddress().toString());
        if (container.getPatientbooking().getSex().equals("Male"))
            ji.selectToggle(male);
        else
            ji.selectToggle(female);
        lbd.setVisible(false);
        lbdlabel.setVisible(false);
        calendar.setVisible(false);
        createappointment.setVisible(false);
        time.setVisible(false);
        clear.setVisible(false);
        ok.setVisible(false);
        save.setVisible(false);
    }

    @ActionMethod("zone")
    public void zone() {

        container.getPatientbooking().setName(name.getText());
        container.getPatientbooking().setAge(Integer.parseInt(age.getText()));
        container.getPatientbooking().setFartherName(farname.getText());
        container.getPatientbooking().setNirc(nirc.getText());
        container.getPatientbooking().setNirc(nirc.getText());
        container.getPatientbooking().setAddress(address.getText());

        PatientBookingDao.mergePatientbooking(container.getPatientbooking());

        container.setPatientbooking(null);
    }
}
